find -name *.bam -print  -exec samtools view {} \; > all_alns.sam
cat all_alns.sam | awk '$2 != 2048 {print}' | awk '$2 != 2064 {print}' > all_alns.primary.sam
