#!/bin/bash

rm -rf 1000_random.bwa.nodes_summary.csv
echo "sra,nb_nodes,nb_nodes_found" >> 1000_random.bwa.nodes_summary.csv
for srr in `cat ~/serratus-batch-assembly/1krandom_missing_graphs.graphs.txt`
do
        if [ ! -d $srr ]
        then
               continue 
        fi
        #lhf=${LHFmap[$srr]}
	lhf=$(grep $srr list_LHF.txt)
	echo "$srr $lhf"
        #cat $lhf
	#nb_lhf=$(grep ">" $lhf |wc -l)
	#echo "$nb_lhf lhfs"
	nb_nodes_found=0
	nb_nodes=0
	samtools view -F 4 $srr/bwamem.bam > $srr/bwamem.bam.txt
	for node in $(grep ">" $lhf | cut -c 2- | awk '{print $1}')
	do
		found=$(grep $node $srr/bwamem.bam.txt |wc -l)
		if [ "$found" -ge "1" ]
		then
			((nb_nodes_found++))
		fi
		echo "$srr $node $nb_aln"
		((nb_nodes++))
	done
	echo "$srr,$nb_nodes,$nb_nodes_found" >> 1000_random.bwa.nodes_summary.csv
done
