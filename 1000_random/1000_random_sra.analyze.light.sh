#!/bin/bash

# a version that only populates nodes_summary
# because full version takes lots of time for some reason

rm -rf 1k_random.pr.light.nodes_summary.csv
echo "sra,nb_nodes,nb_nodes_found" >> 1k_random.pr.light.nodes_summary.csv

for srr in `cat find_finished.txt`
do
        if [ ! -d $srr ]
        then
               continue 
        fi
        #lhf=${LHFmap[$srr]}
	lhf=$(grep $srr list_LHF.txt)
	echo "$srr $lhf"
        #cat $lhf
	#nb_lhf=$(grep ">" $lhf |wc -l)
	#echo "$nb_lhf lhfs"
	nb_nodes_found=0
	nb_nodes=0
	srrls=$(ls -1 $srr/)
	for node in $(grep ">" $lhf | cut -c 2- | awk '{print $1}')
	do
		if [[ $srrls =~ "$node.edges.fa" ]];
		then
			found=1
			((nb_nodes_found++))
		else
			found=0
		fi
		#found=$(ls -1 $srr/$node.edges.fa|wc -l)
		echo $node $found
		((nb_nodes++))
	done
	echo "$srr,$nb_nodes,$nb_nodes_found" >> 1k_random.pr.light.nodes_summary.csv
done
