
#find /rs/motifator_analysis/individual/ | grep contigs|grep LHF.fna > list_LHF.txt
job () {
	srr=$1
	lhf=$(grep $srr list_LHF.txt)
	#ls -l $lhf
	if [ -f $srr/bwamem.bam ]
	then
		#echo "$srr exists"
		return 	
	fi
	bwa index ../1000_random_sra.scaffolds/$srr.rnaviralspades.scaffolds.fasta
	bwa mem -t 1 ../1000_random_sra.scaffolds/$srr.rnaviralspades.scaffolds.fasta $lhf |samtools sort -o tmp_$srr.bam -
	mv tmp_$srr.bam $srr/bwamem.bam
}
export -f job
cat ~/serratus-batch-assembly/1krandom_missing_graphs.graphs.txt |  parallel -j 32 "job {}"
