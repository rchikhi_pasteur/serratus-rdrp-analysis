
find /rs/motifator_analysis/individual/ | grep contigs|grep LHF.fna > list_LHF.txt
job () {
	srr=$1
	lhf=$(grep $srr list_LHF.txt)
	finished=$(grep $srr find_finished.txt)
	#ls -l $lhf
	#if [ -d $srr ]
	if [ "$finished" != "" ]
	then
		return 	
	fi
	mkdir -p $srr
	~/pathracer/bin/pathracer $lhf ../1000_random_sra.assembly_graphs/$srr.rnaviralspades.assembly_graph_with_scaffolds.gfa.gz  -o tmp_$srr --nt -t 16
	mv tmp_$srr/* $srr/
	rmdir tmp_$srr
}
export -f job
cat ~/serratus-batch-assembly/1krandom_missing_graphs.graphs.txt | parallel -j 2 "job {}"
