#!/bin/bash

#find /rs/motifator_analysis/individual/ | grep contigs|grep LHF.fna > list_LHF.txt
rm -rf 1k_random.pr.nodes_scores.csv 1k_random.pr.nodes_summary.csv
echo "sra,node,score,nb_edges" >> 1k_random.pr.nodes_scores.csv
echo "sra,nb_nodes,nb_nodes_found" >> 1k_random.pr.nodes_summary.csv
# some attempt at faster processing
#declare -A LHFmap
#echo "parsing"
#for line in `grep -f ~/serratus-batch-assembly/1krandom_missing_graphs.graphs.txt list_LHF.txt`
#do
#	#echo $line
#	b=$(basename $line)
#	srr=$(echo $b | cut -d"." -f1)
#	LHFmap[$srr]=$b
#	echo $srr $b
#	:
#done
#echo "done"

#for srr in `cat ~/serratus-batch-assembly/1krandom_missing_graphs.graphs.txt`
for srr in `cat find_finished.txt`
do
        if [ ! -d $srr ]
        then
               continue 
        fi
        #lhf=${LHFmap[$srr]}
	lhf=$(grep $srr list_LHF.txt)
	echo "$srr $lhf"
        #cat $lhf
	#nb_lhf=$(grep ">" $lhf |wc -l)
	#echo "$nb_lhf lhfs"
	nb_nodes_found=0
	nb_nodes=0
	for node in $(grep ">" $lhf | cut -c 2- | awk '{print $1}')
	do
		found=$(ls -1 $srr/$node.edges.fa|wc -l)
		nb_aln=0
		if [ "$found" == "1" ]
		then
			((nb_nodes_found++))
			for aln in `grep ">" $srr/$node.seqs.fa`
			do
				nb_edges=$(echo $aln| cut -d"|" -f 2 | awk -F"_" '{print NF}')
				score=$(echo $aln | cut -d"|" -f 1 | cut -d"=" -f 2)
				#echo "score=$score nb_edges=$nb_edges"
				echo "$srr,$node,$score,$nb_edges" >> 1k_random.pr.nodes_scores.csv
				((nb_aln++))
			done
			#echo "$nb_aln alignments"
		else
			echo "$srr,$node,NA,NA" >> 1k_random.pr.nodes_scores.csv
			fi
		echo "$srr $node $nb_aln"
		((nb_nodes++))
	done
	echo "$srr,$nb_nodes,$nb_nodes_found" >> 1k_random.pr.nodes_summary.csv
done
