import csv
c = csv.reader(open('1000_random.pr.nodes_scores.csv'))

from collections import defaultdict
d = defaultdict(list)
a = dict()
for line in c:
    sra,node,score,nb_edges = line
    if score=="NA" or score=="score": continue
    score=float(score)
    d[(sra,node)]+=[score]
    a[(sra,node,score)]=int(nb_edges)


nb_hits_multiple_edges = 0
for hit in d:
    lst = d[hit]
    m = max(lst)
    nb_edges=a[(*hit,m)]
    if nb_edges > 1:
        nb_hits_multiple_edges += 1
print("multi-edges max-score alignments:",nb_hits_multiple_edges)

nb_hits_with_many_good_alignments = 0
for hit in d:
    lst = d[hit]
    m = max(lst)
    many_good_alignments = len([x for x in lst if x != m and x >= m*0.9]) > 0
    if many_good_alignments:
        nb_hits_with_many_good_alignments +=1


print(nb_hits_with_many_good_alignments,len(d))
