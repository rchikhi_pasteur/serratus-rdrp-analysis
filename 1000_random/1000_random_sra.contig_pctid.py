checkv=dict()
for line in open("1000_random_sra.scaffolds_motifator.whole_contigs_hits.fasta_checkv/quality_summary.tsv"):
    # example format:
    #contig_id                                      contig_length   provirus        proviral_length gene_count      viral_genes     host_genes      checkv_quality  miuvig_quality  completeness    completeness_method     contamination   kmer_freq       warnings
    #DRR029069.NODE_1_length_12373_cov_59.139276     12373             No                 NA             4               1               0             High-quality    High-quality    100.0          AAI-based (high-confidence)     0.0     1.0
    ls=line.split()
    quality=ls[7]
    node=ls[0]
    if line.startswith('contig_id'): continue
    if ls[9] == 'NA':
        completeness=None
    else:
        completeness=float(ls[9])

    checkv[node]=(quality,completeness)

virsorter=dict()
for line in open("1000_random_sra.scaffolds_motifator.whole_contigs_hits.fasta_virsorter2/final-viral-score.tsv"):
    #seqname dsDNAphage      NCLDV   RNA     ssDNA   lavidaviridae   max_score       max_score_group length  hallmark        viral   cellular
    #DRR029069.NODE_1_length_12373_cov_59.139276||full       0.713   0.540   1.000   0.227   0.740   1.000   RNA     11458   1       25.000  0.000
    if line.startswith('seqname'): continue
    ls=line.split()
    node=ls[0].split('|')[0]
    score=ls[5]
    if score == 'nan':
        score=None
    else:
        score=float(score)
    virsorter[node]=score


from collections import Counter
res=[]
for line in open("1000_random_sra.contig_pctid.tsv"):
    node,pctid = line.split()
    pctid=float(pctid)
    #get rid of transeq part
    node='_'.join(node.split('_')[:-1])
    #quality,completeness=checkv[node]
    #if pctid==100: 
    #    print(node,checkv[node])
    #res += [quality]
    if pctid==100:
        if node not in virsorter:
            print(node,pctid,"not found by virsorter")
            continue
        score=virsorter[node]
        print(score)

        res +=[score]

print(Counter(res))
