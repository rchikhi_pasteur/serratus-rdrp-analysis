import glob,os,sys

folder="outputs"
if len(sys.argv) > 1:
    folder=sys.argv[1]

for filename in glob.glob(folder + "/*"):
        sra=os.path.basename(filename).split('.')[0]
        for line in open(filename):
                try:
                    a,b,idy = line.split("\t")[:3]
                except:
                    print("error at",filename)
                    exit(1)
                #a = a.replace(' ','_')
                #b = b.replace(' ','_')
                a = a.split(' ')[0] # gets rid of the coords, allows comparison between AA and nt
                b = b.split(' ')[0] 
                t = a[0]+b[0]
                if filename.endswith('r1'):
                    t=t[0]+"r"
                print(sra,a,b,idy,t,sep="\t")
