job () {
	srr=$1
	echo $srr
	micro=$(grep $srr list_LHF_micro.1krandom.txt)
	macro=$(grep $srr list_LHF_macro.1krandom.txt)
	outputs=outputs
	options="-fulldp -maxaccepts 4 -maxrejects 64 -top_hit_only -strand plus -id 0.75 -threads 1"
	## AA
	#micro=$(echo $micro | sed 's/fna$/faa/g')
	#macro=$(echo $macro | sed 's/fna$/faa/g')
	#outputs=outputs_AA
	#options="-fulldp -maxaccepts 4 -maxrejects 64 -top_hit_only -id 0.25 -threads 1" 
	## AA
	echo $micro $macro
	# hack contigs labels
	cat $micro | sed 's/>/>m/g' > $srr.micro
	cat $macro | sed 's/>/>M/g' > $srr.macro
	usearch -usearch_global $srr.micro -db $srr.macro $options  -blast6out $outputs/$srr.mM
	usearch -usearch_global $srr.macro -db $srr.micro $options  -blast6out $outputs/$srr.Mm
	usearch -usearch_global $srr.micro -db rdrp1.udb $options  -blast6out $outputs/$srr.mr1 
	usearch -usearch_global $srr.macro -db rdrp1.udb $options  -blast6out $outputs/$srr.Mr1 
	rm -f $srr.micro $srr.macro
}
export -f job

cat list_1000_random.txt | parallel -j 32 "job {}"
