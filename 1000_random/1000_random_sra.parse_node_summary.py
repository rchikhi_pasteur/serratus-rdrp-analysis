import csv, sys
filename = '1000_random.bwa.nodes_summary.csv'
if len(sys.argv) > 1:
    filename = sys.argv[1]

c = csv.reader(open(filename))

from collections import defaultdict
d = defaultdict(list)
total_nb_nodes = 0
total_nb_nodes_found = 0
for line in c:
    sra,nb_nodes,nb_nodes_found = line
    if nb_nodes=="nb_nodes": continue
    total_nb_nodes += int(nb_nodes)
    total_nb_nodes_found += int(nb_nodes_found)

print(total_nb_nodes,total_nb_nodes_found/total_nb_nodes)

