cores=64

mkdir results
dest=1000_random_sra.scaffolds_motifator
mkdir $dest

source motifator_wrapper.sh

ijob () {
	inpt=$1
	dest=$2
	base=$(basename $inpt)
	srr=$(basename $inpt | cut -f 1 -d '.')
        if [ -f $dest/$base.trim.LHF.faa ] || [ -f $dest/$base.trim.LHF.fna ]
        then
                echo "$srr already motifated"
                return
	else
		echo $srr
        fi
	if [ -s "$inpt" ]
	then
		job $inpt 
	fi
        if [ -s "results/$base.trim.LHF.faa" ] || [ -s "results/$base.trim.LHF.fna" ]
	then
		ls -1 results/$base.trim.LHF.faa results/$base.trim.LHF.fna
		mv results/$base* $dest/
	else
		rm -f results/$base*
	fi
}
export -f ijob

find ./1000_random_sra.scaffolds/ -name "*.fasta"  -not -empty | parallel  --lb -j $cores  "ijob {} $dest"


