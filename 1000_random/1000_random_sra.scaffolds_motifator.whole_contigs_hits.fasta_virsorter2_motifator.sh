cores=2

mkdir results
dest=1000_random_sra.scaffolds_motifator.whole_contigs_hits.fasta_virsorter2_motifator
mkdir $dest

source motifator_wrapper_2021.sh

job 1000_random_sra.scaffolds_motifator.whole_contigs_hits.fasta_virsorter2/final-viral-combined.fa
ls -1 results/
mv results/* $dest/

