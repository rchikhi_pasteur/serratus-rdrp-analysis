onek_ctgs = set()
onek_annot = dict()
for line in open("1k.aggregated.contig_names"):
    if "." not in line: continue
    ls = line.split('.')
    accession = ls[0]
    ctg_name = ls[1]
    full_partial = line.split('|')[-1].strip()
    #print(accession,ctg_name)
    onek_ctgs.add((accession,ctg_name))
    onek_annot[(accession,ctg_name)] = full_partial 

robert_ctgs = set()
for line in open("rdrp_contigs.tsv.contig_names"):
    if "." not in line: continue
    ls = line.split('.')
    accession = ls[0]
    ctg_name = ls[2]
    micro_macro = ls[1]
    if micro_macro == "micro": continue
    #print(accession,ctg_name)
    robert_ctgs.add((accession,ctg_name))

print(len(robert_ctgs),"macro contigs in Robert's rdrp_contigs.tsv")
print(len(onek_ctgs),"contigs in 1krandom")


print(len(onek_ctgs - robert_ctgs),"contigs in 1krandom not in Robert's")
print(len(robert_ctgs- onek_ctgs),"contigs in Robert's macro not in 1krandom")

print(len(robert_ctgs & onek_ctgs),"contigs in both")

nb_full_excl = 0
for c in onek_ctgs - robert_ctgs:
    #print(onek_annot[c])
    if onek_annot[c] == "full":
        nb_full_excl += 1
        if nb_full_excl  < 5: print(c)
print(nb_full_excl,"full contigs in 1krandom aggregated not in Robert's")
