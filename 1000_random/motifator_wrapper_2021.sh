
job () {
	input=$1
	base=$(basename $input)
	./palmscan -threads 8 -rdrp -search_pp $input \
		-report   results/$base.txt \
		-fevout   results/$base.fev \
		-ppout    results/$base.pp.faa \
		-ppout_nt results/$base.pp.nt.fna \
		> /dev/null
}
export -f job 

