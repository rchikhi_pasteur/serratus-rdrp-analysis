contigs=all_new_cov_nido.scaffolds_motifator.whole_contigs_hits.fasta
contigs=1000_random_sra.scaffolds_motifator.whole_contigs_hits.fasta
cores=64

hmmsearch_input=$contigs.transeq.faa
transeq -frame 6 $contigs $hmmsearch_input

splitfolder="$hmmsearch_input"_split/
mkdir $splitfolder
./faSplit sequence $hmmsearch_input $cores $splitfolder/split

for f in `ls $splitfolder/*.fa`
do
	hmm=Pfam-A.hmm
	prefix=$f
	sto=$prefix.sto
	tbl=$prefix.tbl
	domtbl=$prefix.domtbl
	hmmout=$prefix.hmmsearch_stdout
	\time hmmsearch --cpu 1 -A $sto --tblout $tbl --domtblout $domtbl -o $hmmout $hmm $f &
done

wait

cat $splitfolder/*.sto > $hmmsearch_input.sto
cat $splitfolder/*.tbl > $hmmsearch_input.tbl
cat $splitfolder/*.domtbl > $hmmsearch_input.domtbl
cat $splitfolder/*.hmmsearch_stdout > $hmmsearch_input.hmmsearch_stdout

#rm -Rf $splitfolder
