# optional argument: $1 == accession to assemble. must reside in fasta/.
cores=1
asmcores=1
#asmcores=40
#find_filter=("-newermt" "'1/23/2021 13:00:00'")
find_filter=("")
mkdir -p individual

spades=/SPAdes-3.15.5-Linux/bin/rnaviralspades.py

job () {
	se=$1
	spades=$2
	asmcores=$3
	srr=$(basename $se | cut -f 1 -d '.')
	if [ -f individual/$srr.se.before_rr.fasta ]
	then
		echo "$srr already assembled"
		return
	fi
	$spades -t $asmcores -s $se -o tmp_$srr >/dev/null
	ctg=tmp_$srr/contigs.fasta
	if [ -s $ctg ] 
	then
		status=$(wc -l $ctg)
		mv $ctg individual/$srr.se.contigs.fasta
	else
		status=""
	fi
	b4rr=tmp_$srr/before_rr.fasta
	mv $b4rr individual/$srr.se.before_rr.fasta
	echo "$srr $status"
	rm -Rf tmp_$srr
}
export -f job 

eval find fasta -name "$1*.se.fa.gz" ${find_filter[@]} | parallel -j $cores  "job {} $spades $asmcores"

date
echo "now PEs"

job () {
	pe=$1
	spades=$2
	asmcores=$3
	srr=$(basename $pe | cut -f 1 -d '.')
	if [ -f individual/$srr.pe.before_rr.fasta ]
	then
		echo "$srr already assembled"
		return
	fi
	$spades -t $asmcores --12 $pe -o tmp_$srr >/dev/null
	ctg=tmp_$srr/contigs.fasta
	if [ -s $ctg ] 
	then
		status=$(wc -l $ctg)
		mv $ctg individual/$srr.pe.contigs.fasta
	else
		status=""
	fi
	scf=tmp_$srr/scaffolds.fasta
	if [ -s $scf ] 
	then
		statusscf=$(wc -l $scf)
		mv $scf individual/$srr.pe.scaffolds.fasta
	else
		statuscf=""
	fi
	b4rr=tmp_$srr/before_rr.fasta
	mv $b4rr individual/$srr.pe.before_rr.fasta

	echo "$srr $status $statusscf"
	rm -Rf tmp_$srr
}
export -f job 


eval find fasta -name "$1*.pe.fa.gz"  ${find_filter[@]} | parallel -j $cores  "job {} $spades $asmcores"
