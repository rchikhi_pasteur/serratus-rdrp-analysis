cores=90

realign () {
	inpt=$1
	base=$(basename $inpt)
	srr=$(basename $inpt | cut -f 1 -d '.')
	ctg_base=$(echo $base | sed 's/.hits.bed//')
	ctg=../../individual/$ctg_base
	reads_base="$(echo $ctg_base | sed 's/.before_rr.fasta//' | sed 's/.contigs.fasta//').fa.gz"
	reads=../../fasta/$reads_base
	bam=../bam/$(echo $ctg_base | sed 's/.fasta//')
	bam_base=$(basename $bam)
	# check if bam was already created as part of yesterday's run
	#if test "$(find "./bam/$bam_base" -mtime -2)"
	#then
		#echo "skipping $srr"
		#return
	#else
		echo $srr
	#fi
	mkdir tmp_$srr; cd tmp_$srr
	bowtie2-build $ctg $ctg_base >/dev/null
	bowtie2 -x $ctg_base -U $reads -f --end-to-end | samtools sort -o $bam - >/dev/null
	ls -1 $ctg $reads $bam
	cd .. ; rm -Rf tmp_$srr
}
export -f realign 

find ./individual/ -name "*before_rr*.bed"  -not -empty | parallel -j $cores  "realign {}"
#find ./individual/ -name "*contigs*.bed"  -not -empty | parallel -j $cores  "realign {}"


