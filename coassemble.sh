#find fasta -name "*.pe.fa.gz" -print0 | xargs -0 cat > all_pe.fa.gz
#find fasta -name "*.se.fa.gz" -print0 | xargs -0 cat  > all_se.fa.gz

#\time /SPAdes-3.15.0-Linux/bin/rnaviralspades.py --12 all_pe.fa.gz -s all_se.fa.gz -o rnaviralspades_coassembly -t 96 -m 750
#\time /SPAdes-3.15.0-Linux/bin/rnaviralspades.py -o rnaviralspades_coassembly -t 96 -m 740 --restart-from last --read-buffer-size 1024
\time /SPAdes-3.15.0/bin/rnaviralspades.py -o rnaviralspades_coassembly -t 96 -m 740 --restart-from last --read-buffer-size 1024
#-k 33,55,77
