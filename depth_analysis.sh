cores=63

depth () {
	inpt=$1
	base=$(basename $inpt)
	srr=$(echo $base| cut -f 1 -d '.')
	ctg_name=$(echo $base| cut -f 1,2,3 -d '.')
	bam=$inpt
	bed=./individual/"$(echo $base | sed 's/.bam//').fasta.hits.bed"
	depth=./depth/$ctg_name
	echo $srr
	#samtools index $bam
	./mosdepth $depth $bam --by $bed -x -m --thresholds 1,2,3,5,9
	#ls -1 $depth*
}
export -f depth 

#find ./individual/ -name "*before_rr*.bed"  -not -empty | parallel -j $cores  "depth {}"
#find ./individual/ -name "*.bed"  -not -empty | parallel -j $cores  "depth {}"
find ./bam -name "*.bam"  -not -empty | parallel -j $cores  "depth {}"


