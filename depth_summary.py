import sys
import gzip
import os

#print("sra,header,contig_type,p_cvg1,p_cvg2,p_cvg3-4,p_cvg5-8,p_cvg9plus")

ctg_type = "contig" if "contig" in sys.argv[1] else ("before_rr" if "before_rr" in sys.argv[1] else "unknown")
for line in gzip.open(sys.argv[1]):
    # structure: 
    # #chrom  start   end     region  1X      2X      5X      10X     20X     50X     200X
    # NODE_1_length_1481_cov_192.862881       572     899     high-confidence-RdRP    327     327     327     327     327     327     318
    # NODE_2_length_1464_cov_108.554310       568     889     high-confidence-RdRP    321     321     321     321     321     321     284
    line = line.decode()
    if line.startswith('#'): continue
    ls = line.split()
    thresholds = list(map(int,ls[4:]))
    region_name = ls[0]
    region_len = int(ls[2]) - int(ls[1])
    thresholds_names = [1,2,3,5,9]
    threshold = 0
    for i,t in enumerate(thresholds_names):
        if thresholds[i] == region_len:
            threshold=t
    srr=os.path.basename(sys.argv[1]).split('.')[0]
    frac_thresholds=["%.2f"%(float(thresholds[x])/region_len) for x in range(len(thresholds_names))]
    print(srr,region_name,ctg_type,region_len,*frac_thresholds,*thresholds,sep=',')

