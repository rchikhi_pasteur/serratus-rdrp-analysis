import os

version = "1.0"
nb_threads = 512
restart_from_package = 0

#os.system("aws s3 ls s3://serratus-rayan/230120_union_rpro_dpro/individual/ > list.230120_union_rpro_dpro.individual.txt")

# grab just the contigs
datasets = {}
for line in open("list.230120_union_rpro_dpro.individual.txt"):
    if "contigs" not in line: continue
    ls = line.split()
    filename = ls[-1]
    srr = filename.split('.')[0]
    size = int(ls[2])
    if srr in datasets:
        datasets[srr] += [(filename,size)]
    else:
        datasets[srr] = [(filename,size)]

total_size = sum([datasets[srr][0][1] for srr in datasets])
print(len(datasets)," datasets, total size",total_size/1024/1024/1024,"GB")

to_process = list(sorted(list(datasets)))[::-1]
package_datasets = []
package_counter = 0
est_package_size_so_far = 0
package_max_size = 50_000_000_000 # 50 GB
os.system(f"mkdir -p tmp_package")
tsv = open(f"dpro_rpro_v{version}.tsv","w")
def package_filename():
    global version, package_counter
    return f"dpro_rpro_v{version}.pkg{package_counter}.fa"

os.system(f"rm -f {package_filename()} errors.log") # clear file if already present

def add_to_package(srr, dataset, size):
    global est_package_size_so_far, package_counter, package_datasets, tsv
    print(f"adding {dataset} to {package_filename()}, (size {est_package_size_so_far} bytes so far)")
    est_package_size_so_far += size
    package_datasets += [dataset]

def make_archive():
    global est_package_size_so_far, package_counter, package_datasets, tsv
    dllist = open("tmp_package/download.txt","w")
    for dataset in package_datasets:
        dllist.write(dataset +"\n")
    dllist.close()
    print(f"downloading {len(package_datasets)} datasets, package size {est_package_size_so_far} bytes")
    os.system('cat tmp_package/download.txt | parallel -j ' + str(nb_threads) + ' "bash download_and_rename.sh {}"')
    # unsorted
    #os.system(f'find tmp_package/ -name "*contigs.fasta" -exec cat {{}} + > {package_filename()}') # append to package
    os.system(f'find tmp_package/ -name "*contigs.fasta" -print0 | sort -z | xargs -0 -I {{}} cat {{}} > {package_filename()}') # untested, TODO make sure this line is ok and in sync with the tsv
    os.system(f"rm -f tmp_package/download.txt")

    real_package_size_so_far = 0 # we need to reestimate because we've changed the files by appending the srr to headers
    for dataset in package_datasets:
        srr = dataset.split('.')[0] 
        tsv.write(f"{srr}\t{package_filename()}\t{real_package_size_so_far}\n")
        try:
            real_package_size_so_far += os.path.getsize(f"tmp_package/{dataset}")
        except:
            error_file = open("errors.log","a")
            error_file.write(f"could not open tmp_package/{dataset}")
            error_file.close()
    created_size = os.path.getsize(package_filename())
    assert(created_size == real_package_size_so_far)
    
    os.system(f'find tmp_package/ -name "*contigs.fasta" -delete') # clear downloaded files
    os.system(f"rm -f tmp_package/download.txt")

def release_package():
    global est_package_size_so_far, package_counter, package_datasets, tsv
    print(f"releasing {package_filename()} size {est_package_size_so_far} bytes")
    os.system(f"aws s3 cp {package_filename()} s3://serratus-rayan/230120_union_rpro_dpro/packaged/")
    os.system(f"rm -f {package_filename()}")
    est_package_size_so_far = 0
    package_counter += 1
    package_datasets = []

while len(to_process) > 0:
    srr = to_process.pop()
    assert(len(datasets[srr]) == 1)
    dataset, size = datasets[srr][0]
    add_to_package(srr, dataset, size)
    if est_package_size_so_far > package_max_size :
        if package_counter < restart_from_package: 
            # skip this one
            est_package_size_so_far = 0
            package_counter += 1
            package_datasets = []
            continue
        make_archive()
        release_package()

if est_package_size_so_far > 0:
    make_archive()
    release_package()

os.system("rmdir tmp_package")
os.system(f"aws s3 cp dpro_rpro_v{version}.tsv s3://serratus-rayan/230120_union_rpro_dpro/packaged/")
