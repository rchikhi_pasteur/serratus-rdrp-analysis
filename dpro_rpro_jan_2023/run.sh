ID=$1

if [ -z "$ID" ]
then
	echo "specify ID"
	exit 1
fi

mkdir -p fasta

cores=128

job () {
	pro=$1
	acc=$(echo $pro |cut -d. -f1)

	already_done=$(aws s3 ls s3://serratus-rayan/230120_union_rpro_dpro/individual/$acc)
	if [ ! -z "$already_done" ]
	then
		echo "$pro already done"
		exit 0
	fi
	
	echo $pro
	touch dpro_$pro
	touch rpro_$pro
      	aws s3 cp s3://lovelywater/dpro/$pro  .  --quiet
	test -f $pro && mv $pro dpro_$pro
      	aws s3 cp s3://lovelywater/rpro/$pro  .  --quiet
	test -f $pro && mv $pro rpro_$pro

	python3 pro_to_fasta.py <(zcat -f dpro_$pro rpro_$pro) $acc

	bash assemble_individually.sh $acc >/dev/null

	mkdir -p upload_$acc/
	mv individual/$acc.* upload_$acc/ 

	aws s3 cp upload_$acc/ s3://serratus-rayan/230120_union_rpro_dpro/individual/ --recursive --quiet 

	rm -Rf dpro_$pro rpro_$pro upload_$acc/ fasta/$acc.*
}
export -f job   

cat splitted_both_pro/splitted$ID  | parallel --tmpdir $HOME/parallel_tmp -j $cores  "job {}"
	
