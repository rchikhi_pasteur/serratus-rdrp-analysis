import glob
g = open("epsys_120_diamond_cov.out","w")
for f in glob.glob("diamond/*.out"):
    accession = f.split('/')[1].split('.')[0]
    for line in open(f):
        g.write(accession+"_"+line)
g.close()

