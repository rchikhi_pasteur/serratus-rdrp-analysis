import glob
g = open("epsys_120_palmscan.out","w")
for f in glob.glob("res_palmscan/*.fev"):
    accession = f.split('/')[1].split('.')[0]
    for line in open(f):
        before, after = line.split('query=')
        g.write(before + "query=" + accession + "_" + after)
g.close()

