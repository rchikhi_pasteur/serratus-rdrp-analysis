for f in `ls -1 scaffolds/*.fasta`
do
	echo $f
	base=$(basename $f)
	out=diamond/$base.cov.diamond.out
	./run_diamond.sh $f $out
done
