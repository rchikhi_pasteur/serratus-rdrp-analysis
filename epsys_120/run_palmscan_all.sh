for f in `ls -1 scaffolds/*.fasta`
do
	echo $f
	base=$(basename $f)
	out=diamond/$base.cov.diamond.out
        ./palmscan -threads 8 -rdrp -search_pp $f \
                -report   res_palmscan/$base.txt \
                -fevout   res_palmscan/$base.fev \
                -ppout    res_palmscan/$base.pp.faa \
                -ppout_nt res_palmscan/$base.pp.nt.fna \
                > /dev/null
done
