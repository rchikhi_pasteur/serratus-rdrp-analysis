import sys
from pyfasta import Fasta
f = Fasta(sys.argv[1])
g = open(sys.argv[2])
l = dict()
for entry in f:
    s = entry.split()[0]
    l[s]=f[entry]
for entry in g:
    e = entry.strip() 
    print(">%s\n%s"% (e,l[e]))
