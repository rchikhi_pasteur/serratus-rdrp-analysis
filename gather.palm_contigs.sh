cores=32

prefix=1000_random_sra
assembler=rnaviralspades

#prefix=all_new_cov_nido
#assembler=coronaspades

folder=$prefix.scaffolds_motifator
asm_folder=$prefix.scaffolds
res_file=$folder.whole_contigs_hits.hiconf.fasta

rm -f $res_file

job () {
	srr=$1
	folder=$2
	asm_folder=$3
	res_file=$4
	assembler=$5

	echo $srr
	scaf=$asm_folder/$srr.$assembler.scaffolds.fasta
	# slow method
	#if [ ! -f $scaf.fai ]
	#then
	#	samtools faidx $scaf
	#fi
	ctghits=$folder/$srr.$assembler.scaffolds.fasta.hits.contigs.fasta
	echo $ctghits
	#rm -f $ctghits
	#for ctg_name in `cat $folder/$srr.rnaviralspades.scaffolds.fasta.hits.bed | awk '{print $1}'`
	#do
	#	samtools faidx $scaf $ctg_name >> $ctghits
	#done

	# all hits
	#cat $folder/$srr.$assembler.scaffolds.fasta.hits.bed | awk '{print $1}' |sort|uniq > $srr.ctgs
	# only high-confidence hits
	grep "high-confidence" $folder/$srr.$assembler.scaffolds.fasta.hits.bed | awk '{print $1}' |sort|uniq > $srr.ctgs
	
	# when scaffolds are uncompressed
	./faSomeRecords $scaf $srr.ctgs $ctghits
	
	# when scaffolds are compressed
	#unpigz -c $scaf > $srr.tmp
	#./faSomeRecords $srr.tmp $srr.ctgs $ctghits
	#rm -f $srr.tmp
	
	rm -f $srr.ctgs
	sed -i "s/>/>$srr./g" $ctghits
}
export -f job 

all_srr=$(ls $folder | cut -d. -f1 | sort | uniq)
echo -e "${all_srr}" | parallel --lb -j $cores "job {} $folder $asm_folder $res_file $assembler"	

echo "done, concatenating $(echo $all_srr |wc -w) accessions"
for srr in $all_srr
do
	ctghits=$folder/$srr.$assembler.scaffolds.fasta.hits.contigs.fasta
	seqtk seq -A $ctghits >> $res_file  # to have oneline fasta
done
