mkdir -p hmmout palmout tmp

cores=96

job () {
	ctg=$1
	srr=$(basename $ctg | cut -d. -f 1)
        prefix=hmmout/$srr
	#if [ -f $prefix.done ]; then return ; fi
	filename_nt=tmp/$srr.fa
	filename_aa=tmp/$srr.transeq.fa
	echo $srr
	if [ ${ctg: -4} == ".mfc" ]
	then
		/home/ec2-user/serratus-batch-assembly/src/MFCompress-linux64-1.01/MFCompressD -o $filename_nt -t 1 $ctg
	else
		zcat -f $ctg > $filename_nt
	fi
	
	transeq -clean -frame 6 -sequence $filename_nt -outseq $filename_aa >/dev/null

        hmm=RdRP_all.v2.hmm
        sto=$prefix.sto
        tbl=$prefix.tbl
        domtbl=$prefix.domtbl
        hmmout=$prefix.hmmsearch_stdout
        hmmsearch --cpu 1 -A $sto --tblout $tbl --domtblout $domtbl -o $hmmout $hmm $filename_aa

	pprefix=palmout/$srr
	./palmscan -search_pp $filename_nt -rt -rdrp -ppout $pprefix.faa -ppout_nt $pprefix.fna -report $pprefix.txt -fevout $pprefix.fev -threads 1 >/dev/null

	touch $prefix.done

	#cleanup
	rm -f $filename_nt $filename_aa
}
export -f job
#cat list_macro_contigs.txt | parallel -j $cores "job {}"
#cat list_macro_contigs_dups.txt | parallel -j $cores "job {}"
#cat list_macro_contigs_dedup.txt | parallel -j $cores "job {}"

#find /rs/individual/ -type f -name "*.contigs.fasta" > list_micro_contigs.txt
cat list_micro_contigs.txt | parallel -j $cores "job {}"
