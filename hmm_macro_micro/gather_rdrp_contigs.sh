mkdir -p rdrp_contigs

mtype=micro

cores=31

job () {
	ctg=$1
	mtype=$2
	srr=$(basename $ctg | cut -d. -f 1)
        prefix=hmmout_$mtype/$srr
	filename_nt=tmp/$srr.fa
	echo $srr
	if [ ${ctg: -4} == ".mfc" ]
	then
		/home/ec2-user/serratus-batch-assembly/src/MFCompress-linux64-1.01/MFCompressD -o $filename_nt -t 1 $ctg
	else
		zcat -f $ctg > $filename_nt
	fi
	
        sto=$prefix.sto
        tbl=$prefix.tbl
        domtbl=$prefix.domtbl
        hmmout=$prefix.hmmsearch_stdout
	touch tmp/$srr.hmm.contigs.txt
	for ctg in `cat $domtbl |grep -v '^#' |awk '{print $1}' |rev|cut -d"_" -f2- |rev|sort|uniq`
	do
		echo $ctg >> tmp/$srr.hmm.contigs.txt
	done

	touch tmp/$srr.palm.contigs.txt
	for ctg in `cat palmout_$mtype/$srr.fev |awk '{print $2}' |sed 's/query=//g'`
	do
		echo $ctg >> tmp/$srr.palm.contigs.txt
	done

	cat tmp/$srr.hmm.contigs.txt tmp/$srr.palm.contigs.txt | sort | uniq >  tmp/$srr.all.contigs.txt

	./faSomeRecords $filename_nt tmp/$srr.all.contigs.txt rdrp_contigs/$srr.$mtype.fa

	#cleanup
	rm -f tmp/$srr.*
}
export -f job
#cat list_macro_contigs.txt | parallel -j $cores "job {}"
#cat list_macro_contigs_dups.txt | parallel -j $cores "job {}"
#cat list_macro_contigs_dedup.txt | parallel -j $cores "job {} $mtype"

#find /rs/individual/ -type f -name "*.contigs.fasta" > list_micro_contigs.txt
#cat list_micro_contigs.txt | parallel -j $cores "job {} $mtype"


rm -f rdrpplus.micro.fa rdrpplus.macro.fa
for f in `find rdrp_contigs/ -type f`
do
	srr=$(echo $f | cut -d. -f1 |cut -d"/" -f2)
	mtype=$(echo $f | cut -d. -f2)
	echo $srr $mtype
	cat $f | sed "s/^>/>$srr.$mtype./g"  >> rdrpplus.$mtype.fa
done
