
job () {
	input=$1
	base=$(basename $input)
	./motifator  -threads 1 -search_rdrp $input \
		-report results/$base.txt \
		-fevout results/$base.fev \
		-trim_fastaout 	  results/$base.trim.LHF.faa \
		-trim_fastaout_nt results/$base.trim.LHF.fna \
		-bedout           results/$base.hits.bed \
		-motifs_fastaout  results/$base.motifs.fa > /dev/null
}
export -f job 

