cores=30
mkdir results
mkdir individual

source motifator_wrapper.sh

ijob () {
	inpt=$1
	base=$(basename $inpt)
	srr=$(basename $inpt | cut -f 1 -d '.')
	echo $srr
	if [ -s "$inpt" ]
	then
		job $inpt 
	fi
        if [ -s "results/$base.trim.LHF.faa" ] || [ -s "results/$base.trim.LHF.fna" ]
	then
		ls -1 results/$base.trim.LHF.faa results/$base.trim.LHF.fna
		mv results/$base* individual/
	else
		rm -f results/$base*
	fi
}
export -f ijob

find ../individual/ -name "*.fasta"  -not -empty | parallel -j $cores  "ijob {}"


