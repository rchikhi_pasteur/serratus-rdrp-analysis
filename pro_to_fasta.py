#Anton's wizard version:
#zcat $1 | cut -f 1,14,15 | awk 'r!~$1{r=$1;print ">"$1".1";print $2;print ">"$1".2"; print $3}' |gzip > $1.fastq.gz # only for paired-end
#robust version: (v2: supports union of arbitrary number of pro files. v1 was more fragile actually, required both reads of a pair to appear in a pro file -> info loss if 1 read appeared only)
import sys
import gzip
import os
import zlib

def revcomp(st):
    nn = {'A': 'T', 'C': 'G', 'G': 'C', 'T': 'A', 'N': 'N'}
    return "".join(nn[n] for n in reversed(st))

g = None

def print_single(seq_id,read):
    global g
    g.write(f">{seq_id}\n".encode())
    g.write((read+"\n").encode())

previously_printed = None
written = dict()
allowed_chars = set("ACTGN*")
if len(sys.argv) > 2:
    srr_id = sys.argv[2]
else:
    srr_id = os.path.basename(sys.argv[1]).split('.')[0]

for line in (gzip.open(sys.argv[1]) if sys.argv[1].endswith('.gz') else open(sys.argv[1])):
    try:
        line = line.decode()
    except:
        pass
    line = line.split()
    seq_id, read, mate = line[0], line[13], line[14]
    assert(set(mate).issubset(allowed_chars)), f"filename={sys.argv[1]} invalid mate: {mate}"

    if mate != '*':
        if g is None:
            g = gzip.open(f"fasta/{srr_id}.pe.fa.gz","w")
        rcr = revcomp(read)
        canread = min(read,rcr)
        canmate = min(mate,revcomp(mate))
        seq_hash = hex(zlib.crc32(str.encode(min(canread,canmate)+max(canread,canmate))) & 0xffffffff)
        if seq_id in written:
            # we may see the same read twice (when taking the union of two pro files, e.g. rpro and dpro datasets in january 2023)
            assert(written[seq_id] == seq_hash), f'[paired-end] error checking seq hashes of identical seq_ids: seq_id={seq_id} current hash={seq_hash} recorded hash={written[seq_id]}'
        else:
            written[seq_id] = seq_hash

            print_single(seq_id+".1",read)
            print_single(seq_id+".2",mate)
        previously_printed = "pair" 
    else:
        if g is None:
            g = gzip.open(f"fasta/{srr_id}.se.fa.gz","w")
        canread = min(read,revcomp(read))
        seq_hash = hex(zlib.crc32(str.encode(canread)) & 0xffffffff)
        if seq_id in written:
            assert(written[seq_id] == seq_hash), f'[single] error checking seq hashes of identical seq_ids: seq_id={seq_id} current hash={seq_hash} recorded hash={written[seq_id]}'
        else:
            written[seq_id] = seq_hash

            print_single(seq_id,read)
        previously_printed = "single"

    previous_seq_id, previous_read, previous_mate = seq_id, read, mate 
