#/bin/bash

cores=128

mkdir fasta

job () {
	file=$1
	python pro_to_fasta.py $file 
}
export -f job 

find ./pro/ -name "*.pro.gz"  | parallel -j $cores  "job {}"
