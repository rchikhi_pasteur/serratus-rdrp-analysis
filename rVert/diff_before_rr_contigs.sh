find individual -not -path "individual/empty*" | cut -d"/" -f2 | cut -d. -f1 |sort|uniq > list_individual_srr.txt
cores=20

job () {
	srr=$1
	sepe=$(ls -1 individual/$srr* | head -n 1 | cut -d. -f2)
	b4rr=individual/$srr.$sepe.before_rr.fasta
	contigs=individual/$srr.$sepe.contigs.fasta
        if [ ! -f  $b4rr ] || [ ! -f $contigs ]
	then
		continue;
	fi
	diff=$(diff $b4rr $contigs)
	if [ ! -z "$diff" ]
	then
		#echo $srr
		# $diff
		:
	fi
	b4rr_size=$(   wc -c $b4rr   |cut -d" " -f1)
	contigs_size=$(wc -c $contigs|cut -d" " -f1)
	if [ $b4rr_size != $contigs_size ]
	then
		echo $srr $b4rr_size $contigs_size
	fi
}
cat list_individual_srr.txt| parallel -j $cores  "job {}"
