# grabs only some contigs from a microassembly, according to a list of accessions
# MAKE SURE that the contigs are oneline FASTA


# caches the list of accessions
s = set()
for line in open("rdrp1_analysis_og.csv"):
    s.add(line.strip())


header = None
acc = None
#g = open("all.contigs.pp.subselect5.7M.fa","w") 
#for line in open("all.contigs.pp.fa"):
g = open("all.contigs.subselect5.7M.fasta","w") 
for line in open("all.contigs.fasta"):
    if line[0] == '>':
        header = line.strip()
        acc = line[1:].split('_')[0]
    else:
        seq = line.strip()
        if acc in s:
            g.write(f"{header}\n{seq}\n")
g.close()
