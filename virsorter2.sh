conda activate vs2
contigs=1000_random_sra.scaffolds_motifator.whole_contigs_hits.fasta
contigs=1000_random_sra.scaffolds_motifator.trim.LHF.fna
virsorter run -i $contigs -d /serratus-data/virsorter2-db/ -w "$contigs"_virsorter2.redone -j 48
